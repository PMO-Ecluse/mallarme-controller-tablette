from kivy.app import App
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.widget import Widget
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.uix.checkbox import CheckBox
from kivy.uix.button import Button
from kivy.uix.slider import Slider
from kivy.uix.spinner import Spinner
from kivy.properties import StringProperty, ListProperty, NumericProperty, BoundedNumericProperty,  BooleanProperty
from kivy.base import Builder
from kivy.lib.osc import oscAPI

Builder.load_file("MallarmeTablette.kv")

class Onglets(TabbedPanel):
    pass


class ButtonOSC(Button):
    methodOSC = StringProperty(None)
    pathOSC = StringProperty(None)
    destinationIP = StringProperty("127.0.0.1")
    destinationPORT = BoundedNumericProperty(9876, min=1, max=65536, errorvalue=9876)

    def on_press(self):
        oscAPI.sendMsg(self.pathOSC+self.methodOSC,
                dataArray=[1],
                ipAddr=self.destinationIP,
                port=self.destinationPORT)


class SceneButton(Button):
    numeroScene = NumericProperty(0)
    methodOSC = StringProperty("")
    pathOSC = StringProperty("")
    destinationIP = StringProperty("127.0.0.1")
    destinationPORT = BoundedNumericProperty(9876, min=1, max=65536, errorvalue=9876)

    def on_press(self):
        oscAPI.sendMsg(self.pathOSC+self.methodOSC,
                dataArray=[self.numeroScene],
                ipAddr=self.destinationIP,
                port=self.destinationPORT)


class TripleSliderOSC(ButtonBehavior, BoxLayout):
    text = StringProperty("")
    methodOSC = StringProperty("")
    pathOSC = StringProperty("")
    destinationIP = StringProperty("127.0.0.1")
    destinationPORT = BoundedNumericProperty(9876, min=1, max=65536, errorvalue=9876)
    value = ListProperty([0.0, 0.0, 0.0])

    def __init__(self, **kwargs):
        super(TripleSliderOSC, self).__init__(**kwargs)
        self.bind(
            text=TripleSliderOSC.set_text,
            value=TripleSliderOSC.set_value)

    def set_text(self, aText):
        self._label.text = aText

    def set_value(self, listeValeur):
        oscAPI.sendMsg(self.pathOSC+self.methodOSC,
                dataArray=listeValeur,
                ipAddr=self.destinationIP,
                port=self.destinationPORT)


class Element(ButtonBehavior, BoxLayout):
    text = StringProperty("")
    pathOSC = StringProperty("")
    destinationIP = StringProperty("127.0.0.1")
    destinationPORT = BoundedNumericProperty(9876, min=1, max=65536, errorvalue=9876)

    def __init__(self, **kwargs):
        super(Element, self).__init__(**kwargs)
        self.bind(
            text=Element.set_text,
            pathOSC=Element.set_pathOSC,
            destinationIP=Element.set_destinationIP,
            destinationPORT=Element.set_destinationPORT)

    def set_text(self, aText):
        self.label.text = aText

    def set_pathOSC(self, aPath):
        self.dessinable.pathOSC = aPath
        self.apparait.pathOSC = aPath
        self.lineWidth.pathOSC = aPath
#        self.trace.pathOSC = aPath

    def set_destinationIP(self, aIP):
        self.dessinable.destinationIP = aIP
        self.apparait.destinationIP = aIP
        self.lineWidth.destinationIP = aIP
#        self.trace.destinationIP = aIP

    def set_destinationPORT(self, aPORT):
        self.dessinable.destinationPORT = aPORT
        self.apparait.destinationPORT = aPORT
        self.lineWidth.destinationPORT = aPORT
#        self.trace.destinationPORT = aPORT


class SpinnerOSC(Spinner):
    methodOSC = StringProperty("")
    pathOSC = StringProperty("")
    destinationIP = StringProperty("127.0.0.1")
    destinationPORT = BoundedNumericProperty(9876, min=1, max=65536, errorvalue=9876)
    map = {"Ligne":0,  "Pointilles":1,  "Plein":2}

    def __init__(self, **kwargs):
        super(SpinnerOSC, self).__init__(**kwargs)
        self.bind(text=self.set_text)

    def set_text(self, spinner, text):
        oscAPI.sendMsg(self.pathOSC+self.methodOSC,
                dataArray=[self.map[text]],
                ipAddr=self.destinationIP,
                port=self.destinationPORT)


class ListeItemSliderOSC(ButtonBehavior, GridLayout):
    text = StringProperty("")
    methodOSC = StringProperty("")
    pathOSC = StringProperty("")
    destinationIP = StringProperty("127.0.0.1")
    destinationPORT = BoundedNumericProperty(9876, min=1, max=65536, errorvalue=9876)
    min = NumericProperty(1)
    max = NumericProperty(1)
    step = NumericProperty(1)
    value = NumericProperty(1)

    def __init__(self, **kwargs):
        super(ListeItemSliderOSC, self).__init__(**kwargs)
        self.bind(
            text=ListeItemSliderOSC.set_text,
            methodOSC=ListeItemSliderOSC.set_methodOSC,
            pathOSC=ListeItemSliderOSC.set_pathOSC,
            destinationIP=ListeItemSliderOSC.set_destinationIP,
            destinationPORT=ListeItemSliderOSC.set_destinationPORT,
            min=ListeItemSliderOSC.set_min,
            max=ListeItemSliderOSC.set_max,
            step=ListeItemSliderOSC.set_step,
            value=ListeItemSliderOSC.set_value)

    def set_text(self, aText):
        self._label.text = aText

    def set_methodOSC(self, aMethod):
        self._slider.methodOSC = aMethod

    def set_pathOSC(self, aPath):
        self._slider.pathOSC = aPath

    def set_destinationIP(self, aIP):
        self._slider.destinationIP = aIP

    def set_destinationPORT(self, aPORT):
        self._slider.destinationPORT = aPORT

    def set_min(self, valeur):
        self._slider.min = valeur

    def set_max(self, valeur):
        self._slider.max = valeur

    def set_step(self, valeur):
        self._slider.step = valeur

    def set_value(self, valeur):
        self._slider.value = valeur


class SliderOSC(Slider):
    methodOSC = StringProperty(None)
    pathOSC = StringProperty(None)
    destinationIP = StringProperty("127.0.0.1")
    destinationPORT = BoundedNumericProperty(9876, min=1, max=65536, errorvalue=9876)

    def __init__(self, **kwargs):
        super(SliderOSC, self).__init__(**kwargs)
        self.bind(
            value=SliderOSC.on_slider_value)

    def on_slider_value(self, value):
        oscAPI.sendMsg(self.pathOSC+self.methodOSC,
                dataArray=[value],
                ipAddr=self.destinationIP,
                port=self.destinationPORT)


class ListeItemCheckboxOSC(ButtonBehavior, GridLayout):
    text = StringProperty("")
    methodOSC = StringProperty("")
    pathOSC = StringProperty("")
    destinationIP = StringProperty("127.0.0.1")
    destinationPORT = BoundedNumericProperty(9876, min=1, max=65536, errorvalue=9876)
    value = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(ListeItemCheckboxOSC, self).__init__(**kwargs)
        self.bind(
            text=ListeItemCheckboxOSC.set_text,
            methodOSC=ListeItemCheckboxOSC.set_methodOSC,
            pathOSC=ListeItemCheckboxOSC.set_pathOSC,
            destinationIP=ListeItemCheckboxOSC.set_destinationIP,
            destinationPORT=ListeItemCheckboxOSC.set_destinationPORT,
            value = ListeItemCheckboxOSC.set_value)

    def set_text(self, aText):
        self._label.text = aText

    def set_methodOSC(self, aMethod):
        self._checkbox.methodOSC = aMethod

    def set_pathOSC(self, aPath):
        self._checkbox.pathOSC = aPath

    def set_destinationIP(self, aIP):
        self._checkbox.destinationIP = aIP

    def set_destinationPORT(self, aPORT):
        self._checkbox.destinationPORT = aPORT
        
    def set_value(self, aValue):
        self._checkbox.active = aValue


class CheckboxOSC(CheckBox):
    methodOSC = StringProperty("")
    pathOSC = StringProperty("")
    destinationIP = StringProperty("127.0.0.1")
    destinationPORT = BoundedNumericProperty(9876, min=1, max=65536, errorvalue=9876)

    def __init__(self, **kwargs):
        super(CheckboxOSC, self).__init__(**kwargs)
        self.bind(
            active=CheckboxOSC.on_checkbox_active)

    def on_checkbox_active(self, value):
        valueOSC = 1 if value else 0
        oscAPI.sendMsg(self.pathOSC+self.methodOSC,
                dataArray=[valueOSC],
                ipAddr=self.destinationIP,
                port=self.destinationPORT)


class ControlPoint(Widget):
    cross = ListProperty([0, 0, 0, 0, 0, 0, 0, 0])
    methodOSC = StringProperty("")
    pathOSC = StringProperty("")
    destinationIP = StringProperty("127.0.0.1")
    destinationPORT = BoundedNumericProperty(9876, min=1, max=65536, errorvalue=9876)

    def update_cross(self, touch):
        self.cross = [
            touch.x, 0, touch.x, self.parent.top,
            self.parent.x, touch.y, self.parent.right, touch.y]

    def on_pos(self, instance, value):
        if self.parent != None:
            oscAPI.sendMsg(self.pathOSC+self.methodOSC,
                dataArray=[
                    (self.center_x-self.parent.x)/self.parent.width,
                    (self.parent.height - self.center_y)/self.parent.height],
                ipAddr=self.destinationIP,
                port=self.destinationPORT)

    def on_touch_down(self, touch):
        if touch.x > self.parent.x and touch.y > 0:
            if self.collide_point(touch.x, touch.y):
                self.center = touch.pos
                touch.grab(self)
                self.update_cross(touch)

    def on_touch_move(self, touch):
        if touch.grab_current is self:
            if touch.x > self.parent.x and self.parent.top > touch.y > 0:
                self.center = touch.pos
                self.update_cross(touch)
        else:
            return False

    def on_touch_up(self, touch):
        if touch.grab_current is self:
            touch.ungrab(self)
        else:
            pass


class Controller(FloatLayout):
    pass


class ControllerApp(App):
    def on_pause(self):
        return True

    def on_resume(self):
        pass

    def build(self):
        oscAPI.init()
        controller = Controller()
        return controller


if __name__ == '__main__':
    ControllerApp().run()
